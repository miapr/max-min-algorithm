﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MaxMinAlgorithm
{
    internal class MaxDistancePoint
    {
        public double Distance { get; set; }
        public Point Point { get; set; }
    }

    internal class MaxMinAlgorithm
    {
        private readonly Point[] _points;

        private readonly List<PointsArea> _pointsAreas;

        public bool IsChanged = true;

        public MaxMinAlgorithm(int pointNumber)
        {
            _points = new Point[pointNumber];
            _pointsAreas = new List<PointsArea>();
            InitializePointsAreas();
        }

        private void InitializePointsAreas()
        {
            for (var i = 0; i < _pointsAreas.Count; i++)
            {
                _pointsAreas[i] = new PointsArea();
            }
        }

        private void CreateRandomPoints(int width, int height)
        {
            var randomCoordinate = new Random();

            for (var i = 0; i < _points.Length; i++)
            {
                _points[i] = new Point(randomCoordinate.Next(width), randomCoordinate.Next(height));
            }
        }

        private void ChooseTwoCorePoints()
        {
            var randomCore = new Random();

            _pointsAreas.Add(new PointsArea {Core = _points[randomCore.Next(_points.Length)]});
            _pointsAreas.Add(new PointsArea {Core = FindNewCoreCandidate().Point});
            DefineAreas();
        }

        private double CountDistance(Point core, Point point)
        {
            return Math.Sqrt(Math.Pow(core.X - point.X, 2) + Math.Pow(core.Y - point.Y, 2));
        }

        private int FindMinimalDistance(Point point)
        {
            var minimalDistanceIndex = 0;

            for (var i = 0; i < _pointsAreas.Count; i++)
            {
                if (CountDistance(_pointsAreas[i].Core, point) <
                    CountDistance(_pointsAreas[minimalDistanceIndex].Core, point))
                    minimalDistanceIndex = i;
            }

            return minimalDistanceIndex;
        }

        private void DefineAreas()
        {
            foreach (var pointArea in _pointsAreas)
            {
                pointArea.Points.Clear();
            }

            foreach (var point in _points)
            {
                _pointsAreas[FindMinimalDistance(point)].Points.Add(point);
            }
        }

        public List<PointsArea> StartAlgorithm(int width, int height)
        {
            CreateRandomPoints(width, height);
            ChooseTwoCorePoints();

            return _pointsAreas;
        }

        public List<PointsArea> GetNewCore()
        {
            var newCoreCandidate = FindNewCoreCandidate();
            if (CheckNewCore(newCoreCandidate.Distance))
            {
                _pointsAreas.Add(new PointsArea() {Core = newCoreCandidate.Point});
            }
            else
            {
                IsChanged = false;
            }

            DefineAreas();

            return _pointsAreas;
        }

        private MaxDistancePoint FindNewCoreCandidate()
        {
            var newCoreCandidate = new MaxDistancePoint { Distance = 0, Point = new Point() };
            foreach (var pointsArea in _pointsAreas)
            {
                var maxDistancePoint = CountMaxDistanceInArea(pointsArea);
                if (maxDistancePoint.Distance > newCoreCandidate.Distance)
                    newCoreCandidate = maxDistancePoint;
            }

            return newCoreCandidate;
        }

        private MaxDistancePoint CountMaxDistanceInArea(PointsArea pointsArea)
        {
            var maxDistancePoint = new MaxDistancePoint { Distance = 0, Point = new Point() };
            foreach (var point in pointsArea.Points)
            {
                var distance = CountDistance(pointsArea.Core, point);
                if (maxDistancePoint.Distance < distance)
                {
                    maxDistancePoint.Distance = distance;
                    maxDistancePoint.Point = point;
                }
            }

            return maxDistancePoint;
        }

        private bool CheckNewCore(double maxDistance)
        {
            return CountAverageCoreDistance() / 2 < maxDistance;
        }

        private double CountAverageCoreDistance()
        {
            double sumDistances = 0;
            var count = 0;
            for (var i = 0; i < _pointsAreas.Count; i++)
            {
                for (var j = i+1; j < _pointsAreas.Count; j++)
                {
                    sumDistances += CountDistance(_pointsAreas[i].Core, _pointsAreas[j].Core);
                    count++;
                }
            }

            return sumDistances / count;
        }
    }
}