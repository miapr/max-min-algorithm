﻿using System;
using System.Windows.Forms;

namespace MaxMinAlgorithm
{
    public partial class MainForm : Form
    {
        private AlgorithmVisualization _algorithmVisualization;
        public MainForm()
        {
            InitializeComponent();
        }

        private void countButton_Click(object sender, EventArgs e)
        {
            int nNumber;
            if (int.TryParse(nNumberTextBox.Text, out nNumber) &&
                nNumber >= 1000 && nNumber <= 100000)
            {
                _algorithmVisualization = new AlgorithmVisualization(nNumber);

                using (var graphics = algorithmBox.CreateGraphics())
                {
                    _algorithmVisualization.FindAreas(graphics, algorithmBox.Width, algorithmBox.Height);
                }

                defineAreasButton.Enabled = true;
            }
            else
            {
                MessageBox.Show("Проверьте введенные данные!");
            }
        }

        private void defineAreasButton_Click(object sender, EventArgs e)
        {
            using (var graphics = algorithmBox.CreateGraphics())
            {
                _algorithmVisualization.DefineAreas(graphics);
            }

            defineAreasButton.Enabled = false;
        }
    }
}
