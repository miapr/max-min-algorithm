﻿namespace MaxMinAlgorithm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.algorithmBox = new System.Windows.Forms.PictureBox();
            this.countButton = new System.Windows.Forms.Button();
            this.nNumberTextBox = new System.Windows.Forms.TextBox();
            this.infoLabel = new System.Windows.Forms.Label();
            this.defineAreasButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmBox)).BeginInit();
            this.SuspendLayout();
            // 
            // algorithmBox
            // 
            this.algorithmBox.BackColor = System.Drawing.Color.White;
            this.algorithmBox.Location = new System.Drawing.Point(38, 44);
            this.algorithmBox.Name = "algorithmBox";
            this.algorithmBox.Size = new System.Drawing.Size(762, 431);
            this.algorithmBox.TabIndex = 0;
            this.algorithmBox.TabStop = false;
            // 
            // countButton
            // 
            this.countButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.countButton.Location = new System.Drawing.Point(395, 6);
            this.countButton.Name = "countButton";
            this.countButton.Size = new System.Drawing.Size(99, 23);
            this.countButton.TabIndex = 1;
            this.countButton.Text = "Посчитать";
            this.countButton.UseVisualStyleBackColor = true;
            this.countButton.Click += new System.EventHandler(this.countButton_Click);
            // 
            // nNumberTextBox
            // 
            this.nNumberTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nNumberTextBox.Location = new System.Drawing.Point(279, 6);
            this.nNumberTextBox.Name = "nNumberTextBox";
            this.nNumberTextBox.Size = new System.Drawing.Size(100, 22);
            this.nNumberTextBox.TabIndex = 2;
            this.nNumberTextBox.Text = "10000";
            // 
            // infoLabel
            // 
            this.infoLabel.AutoSize = true;
            this.infoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoLabel.Location = new System.Drawing.Point(35, 9);
            this.infoLabel.Name = "infoLabel";
            this.infoLabel.Size = new System.Drawing.Size(238, 16);
            this.infoLabel.TabIndex = 6;
            this.infoLabel.Text = "Количество образов (от 1к до 100к)";
            // 
            // defineAreasButton
            // 
            this.defineAreasButton.Enabled = false;
            this.defineAreasButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.defineAreasButton.Location = new System.Drawing.Point(510, 6);
            this.defineAreasButton.Name = "defineAreasButton";
            this.defineAreasButton.Size = new System.Drawing.Size(127, 23);
            this.defineAreasButton.TabIndex = 7;
            this.defineAreasButton.Text = "Распределить";
            this.defineAreasButton.UseVisualStyleBackColor = true;
            this.defineAreasButton.Click += new System.EventHandler(this.defineAreasButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 496);
            this.Controls.Add(this.defineAreasButton);
            this.Controls.Add(this.infoLabel);
            this.Controls.Add(this.nNumberTextBox);
            this.Controls.Add(this.countButton);
            this.Controls.Add(this.algorithmBox);
            this.Name = "MainForm";
            this.Text = "Алгоритм максимина";
            ((System.ComponentModel.ISupportInitialize)(this.algorithmBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox algorithmBox;
        private System.Windows.Forms.Button countButton;
        private System.Windows.Forms.TextBox nNumberTextBox;
        private System.Windows.Forms.Label infoLabel;
        private System.Windows.Forms.Button defineAreasButton;
    }
}

