﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace MaxMinAlgorithm
{
    public class AlgorithmVisualization
    {
        private readonly MaxMinAlgorithm _maxMinAlgorithm;

        private KmeansAlgorithm _kmeansAlgorithm;

        private List<PointsArea> _pointsAreas;

        private const int PointWidth = 5;

        private readonly Color[] _spotColors = new Color[20];

        public AlgorithmVisualization(int pointNubmer)
        {
            _maxMinAlgorithm = new MaxMinAlgorithm(pointNubmer);
            GenerateRandomColors();
        }

        private void GenerateRandomColors()
        {
            var random = new Random();
            for (var i = 0; i < _spotColors.Length; i++)
            {
                _spotColors[i] = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }

        }

        public void FindAreas(Graphics graphics, int width, int height)
        {
            graphics.Clear(Color.White);
             _pointsAreas = _maxMinAlgorithm.StartAlgorithm(width, height);

            while (_maxMinAlgorithm.IsChanged)
            {

                _maxMinAlgorithm.GetNewCore();
            }

            for (var i = 0; i < _pointsAreas.Count; i++)
            {
                DrawPoints(graphics, _pointsAreas[i], _spotColors[i]);
            }

        }

        public void DefineAreas(Graphics graphics)
        {
            var pointsAreas = _pointsAreas.ToArray();
            _kmeansAlgorithm = new KmeansAlgorithm(pointsAreas);

            while (_kmeansAlgorithm.IsChanged)
            { 
                _kmeansAlgorithm.ImprovePointsAreas();
            }

            for (var i = 0; i < pointsAreas.Length; i++)
            {
                DrawPoints(graphics, pointsAreas[i], _spotColors[i]);
            }
        }

        private void DrawPoints(Graphics graphics, PointsArea pointArea, Color color)
        {
            using (var brush = new SolidBrush(color))
            {
                foreach (var point in pointArea.Points)
                {
                    graphics.FillEllipse(brush, point.X, point.Y, PointWidth, PointWidth);
                }

                brush.Color = Color.Black;
                graphics.FillEllipse(brush, pointArea.Core.X, pointArea.Core.Y, PointWidth, PointWidth);
            }
        }
    }
}